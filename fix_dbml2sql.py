import os, re, argparse

parentheses_pat = re.compile(r"\(|\)")
detect_string = re.compile(r"DEFAULT \(CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP\)")

def read_file(file_path):
    with open(file_path, 'r') as f:
        for content in f.readlines():
            if detect_string.search(content):
                content = parentheses_pat.sub('', content)
            yield content

def fix_sql_file(file_path, file_prefix='fixed'):
    contents = read_file(file_path)
    dir_path, file_name = os.path.split(file_path)
    if not file_prefix:
        raise ValueError("file_prefix can't be empty string.")
    file_name = '_'.join([file_prefix, file_name])
    file_path = os.path.join(dir_path, file_name)
    with open(file_path, 'w') as f:
        for content in contents:
            f.write(content)
    print(f'Successfully save fixed file: {file_path}')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', dest='filepath')
    args = parser.parse_args()
    file_path = args.filepath

    if file_path:
        if os.path.exists(file_path):
            fix_sql_file(file_path)
        else:
            raise ValueError(f"The file doesn't exist in {file_path}")
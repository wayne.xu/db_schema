-- SQL dump generated using DBML (dbml-lang.org)
-- Database: MySQL
-- Generated at: 2020-10-21T02:56:14.289Z

CREATE TABLE `category` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `country_id` int(10) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `league` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(128) DEFAULT null,
  `name_en` varchar(128) DEFAULT null,
  `logo` varchar(64) DEFAULT null,
  `confederation` varchar(64) DEFAULT null,
  `category_id` int(64),
  `parent_id` int(10) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `league_match_info` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(256) DEFAULT null,
  `name_en` varchar(256) DEFAULT null,
  `parent_id` int(10) DEFAULT null,
  `match_type` varchar(64) DEFAULT null,
  `description` text DEFAULT null,
  `start_at` varchar(64) DEFAULT null,
  `end_at` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `arena` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `open_year` varchar(64) DEFAULT null,
  `country_id` varchar(64) DEFAULT null,
  `city_name` varchar(64) DEFAULT null,
  `address` varchar(128) DEFAULT null,
  `capacity` varchar(64) DEFAULT null,
  `map_coordinates` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `country` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `abbreviation` varchar(64) DEFAULT null,
  `code` varchar(64) UNIQUE DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `currency` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `team` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `found` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `season_teams` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `league_match_info_id` int(10),
  `team_id` int(10),
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `arena_id` int(10) DEFAULT null,
  `country_id` int(10) DEFAULT null,
  `logo` varchar(64) DEFAULT null,
  `market_value` varchar(64) DEFAULT null,
  `currency_id` int(10),
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `team_season_standing` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `league_match_info_id` int(10),
  `season_team_id` int(10),
  `points` varchar(64) DEFAULT "",
  `rank` varchar(64) DEFAULT "",
  `played` varchar(64) DEFAULT "",
  `win` varchar(64) DEFAULT "",
  `draw` varchar(64) DEFAULT "",
  `loss` varchar(64) DEFAULT "",
  `goals` varchar(64) DEFAULT "",
  `goals_against` varchar(64) DEFAULT "",
  `goals_diff` varchar(64) DEFAULT "",
  `home_rank` varchar(64) DEFAULT "",
  `home_points` varchar(64) DEFAULT "",
  `home_position` varchar(64) DEFAULT "",
  `home_played` varchar(64) DEFAULT "",
  `home_win` varchar(64) DEFAULT "",
  `home_draw` varchar(64) DEFAULT "",
  `home_loss` varchar(64) DEFAULT "",
  `home_goals` varchar(64) DEFAULT "",
  `home_goals_against` varchar(64) DEFAULT "",
  `home_goals_diff` varchar(64) DEFAULT "",
  `away_rank` varchar(64) DEFAULT "",
  `away_points` varchar(64) DEFAULT "",
  `away_position` varchar(64) DEFAULT "",
  `away_played` varchar(64) DEFAULT "",
  `away_win` varchar(64) DEFAULT "",
  `away_draw` varchar(64) DEFAULT "",
  `away_loss` varchar(64) DEFAULT "",
  `away_goals` varchar(64) DEFAULT "",
  `away_goals_against` varchar(64) DEFAULT "",
  `away_goals_diff` varchar(64) DEFAULT "",
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `team_season_stats` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `league_match_info_id` int(10),
  `season_team_id` int(10),
  `matches_played` varchar(64) DEFAULT "",
  `shots_on_target` varchar(64) DEFAULT "",
  `shots_total` varchar(64) DEFAULT "",
  `shots_off_target` varchar(64) DEFAULT "",
  `shots_on_post` varchar(64) DEFAULT "",
  `shots_on_bar` varchar(64) DEFAULT "",
  `shots_blocked` varchar(64) DEFAULT "",
  `corner_kicks` varchar(64) DEFAULT "",
  `average_ball_possession` varchar(64) DEFAULT "",
  `free_kicks` varchar(64) DEFAULT "",
  `offsides` varchar(64) DEFAULT "",
  `goals_by_foot` varchar(64) DEFAULT "",
  `goals_by_head` varchar(64) DEFAULT "",
  `goals_scored` varchar(64) DEFAULT "",
  `goals_conceded` varchar(64) DEFAULT "",
  `goals_scored_first_half` varchar(64) DEFAULT "",
  `goals_scored_second_half` varchar(64) DEFAULT "",
  `goals_conceded_first_half` varchar(64) DEFAULT "",
  `goals_conceded_second_half` varchar(64) DEFAULT "",
  `penalty` varchar(64) DEFAULT "",
  `penalties_missed` varchar(64) DEFAULT "",
  `yellow_red_cards` varchar(64) DEFAULT "",
  `yellow_cards` varchar(64) DEFAULT "",
  `red_cards` varchar(64) DEFAULT "",
  `cards_given` varchar(64) DEFAULT "",
  `assists` varchar(64) DEFAULT "",
  `clearances` varchar(64) DEFAULT "",
  `interceptions` varchar(64) DEFAULT "",
  `tackles` varchar(64) DEFAULT "",
  `dribble` varchar(64) DEFAULT "",
  `dribble_succ` varchar(64) DEFAULT "",
  `passes` varchar(64) DEFAULT "",
  `passes_accuracy` varchar(64) DEFAULT "",
  `key_passes` varchar(64) DEFAULT "",
  `crosses` varchar(64) DEFAULT "",
  `crosses_accuracy` varchar(64) DEFAULT "",
  `long_balls` varchar(64) DEFAULT "",
  `long_balls_accuracy` varchar(64) DEFAULT "",
  `duels` varchar(64) DEFAULT "",
  `duels_won` varchar(64) DEFAULT "",
  `fouls` varchar(64) DEFAULT "",
  `was_fouled` varchar(64) DEFAULT "",
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `team_season_stats_top` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `league_match_info_id` int(10),
  `season_team_id` int(10),
  `stat_type_id` int(10),
  `stat_value` varchar(64) NOT NULL,
  `stat_rank` varchar(64) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `team_stats_type` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `sort` int(10) NOT NULL,
  `stat_name_cn` varchar(64),
  `stat_name_en` varchar(64),
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `team_season_event` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `team_event_type_id` int(10),
  `season_team_id` int(10),
  `description` text DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `team_event_type` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `position` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `abbreviation` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `date_of_birth` varchar(64) DEFAULT null,
  `place_of_birth` varchar(64) DEFAULT null,
  `height` varchar(64) DEFAULT null,
  `weight` varchar(64) DEFAULT null,
  `experience` varchar(64) DEFAULT null,
  `position_id` int(10),
  `nationality` varchar(64) DEFAULT null,
  `preferred_foot` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `season_team_roster` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `season_team_id` int(10),
  `player_id` int(10),
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `position_id` int(10),
  `portrait` varchar(64) DEFAULT null,
  `jersey_num` varchar(64) DEFAULT null,
  `salary` varchar(64) DEFAULT null,
  `market_value` varchar(64) DEFAULT null,
  `currency_id` int(10),
  `saves_pts` varchar(64) DEFAULT null,
  `saves_pts_avg` varchar(64) DEFAULT null,
  `anticipation_pts` varchar(64) DEFAULT null,
  `anticipation_pts_avg` varchar(64) DEFAULT null,
  `ball_distribution_pts` varchar(64) DEFAULT null,
  `ball_distribution_pts_avg` varchar(64) DEFAULT null,
  `aerial_pts` varchar(64) DEFAULT null,
  `aerial_pts_avg` varchar(64) DEFAULT null,
  `tactical_pts` varchar(64) DEFAULT null,
  `tactical_pts_avg` varchar(64) DEFAULT null,
  `attacking_pts` varchar(64) DEFAULT null,
  `attacking_pts_avg` varchar(64) DEFAULT null,
  `defending_pts` varchar(64) DEFAULT null,
  `defending_pts_avg` varchar(64) DEFAULT null,
  `creativity_pts` varchar(64) DEFAULT null,
  `creativity_pts_avg` varchar(64) DEFAULT null,
  `technical_pts` varchar(64) DEFAULT null,
  `technical_pts_avg` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player_season_stats` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `league_match_info_id` int(10),
  `season_team_id` int(10),
  `season_player_id` int(10),
  `position_id` int(10),
  `matches_played` varchar(64) DEFAULT "",
  `minutes_played` varchar(64) DEFAULT "",
  `goals_scored` varchar(64) DEFAULT "",
  `goals_by_penalty` varchar(64) DEFAULT "",
  `goals_by_head` varchar(64) DEFAULT "",
  `goals_conceded` varchar(64) DEFAULT "",
  `own_goals` varchar(64) DEFAULT "",
  `assists` varchar(64) DEFAULT "",
  `yellow_cards` varchar(64) DEFAULT "",
  `yellow_red_cards` varchar(64) DEFAULT "",
  `red_cards` varchar(64) DEFAULT "",
  `cards_given` varchar(64) DEFAULT "",
  `clearances` varchar(64) DEFAULT "",
  `interceptions` varchar(64) DEFAULT "",
  `corner_kicks` varchar(64) DEFAULT "",
  `shots_on_target` varchar(64) DEFAULT "",
  `shots_off_target` varchar(64) DEFAULT "",
  `shots_blocked` varchar(64) DEFAULT "",
  `shots_faced` varchar(64) DEFAULT "",
  `penalties` varchar(64) DEFAULT "",
  `penalties_missed` varchar(64) DEFAULT "",
  `substituted_in` varchar(64) DEFAULT "",
  `substituted_out` varchar(64) DEFAULT "",
  `long_passes_total` varchar(64) DEFAULT "",
  `long_passes_successful` varchar(64) DEFAULT "",
  `long_passes_unsuccessful` varchar(64) DEFAULT "",
  `passes_total` varchar(64) DEFAULT "",
  `passes_successful` varchar(64) DEFAULT "",
  `passes_unsuccessful` varchar(64) DEFAULT "",
  `crosses_total` varchar(64) DEFAULT "",
  `crosses_successful` varchar(64) DEFAULT "",
  `tackles_total` varchar(64) DEFAULT "",
  `tackles_successful` varchar(64) DEFAULT "",
  `offsides` varchar(64) DEFAULT "",
  `clean_sheets` varchar(64) DEFAULT "",
  `chances_created` varchar(64) DEFAULT "",
  `loss_of_possession` varchar(64) DEFAULT "",
  `diving_saves` varchar(64) DEFAULT "",
  `rating` varchar(64) DEFAULT "",
  `first` varchar(64) DEFAULT "",
  `shots` varchar(64) DEFAULT "",
  `dribble` varchar(64) DEFAULT "",
  `dribble_succ` varchar(64) DEFAULT "",
  `key_passes` varchar(64) DEFAULT "",
  `duels` varchar(64) DEFAULT "",
  `duels_won` varchar(64) DEFAULT "",
  `dispossessed` varchar(64) DEFAULT "",
  `fouls` varchar(64) DEFAULT "",
  `fouls_committed` varchar(64) DEFAULT "",
  `was_fouled` varchar(64) DEFAULT "",
  `saves` varchar(64) DEFAULT "",
  `punches` varchar(64) DEFAULT "",
  `runs_out` varchar(64) DEFAULT "",
  `runs_out_succ` varchar(64) DEFAULT "",
  `go_high_claim` varchar(64) DEFAULT "",
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player_season_stats_top` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_id` int(10),
  `league_match_info_id` int(10),
  `season_team_id` int(10),
  `season_player_id` int(10),
  `stat_type_id` int(64),
  `stat_value` varchar(64) NOT NULL,
  `stat_rank` varchar(64) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player_stats_type` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `stat_name_cn` varchar(64),
  `stat_name_en` varchar(64),
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player_season_event` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `league_match_info` int(10),
  `player_id` int(10),
  `event_type_id` int(10),
  `from_team` int(10),
  `to_team` int(10),
  `start_time` varchar(64) DEFAULT null,
  `end_time` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player_event_type` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `event_name` varchar(64) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `coach` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `date_of_birth` varchar(64) DEFAULT null,
  `place_of_birth` varchar(64) DEFAULT null,
  `height` varchar(64) DEFAULT null,
  `weight` varchar(64) DEFAULT null,
  `experience` varchar(64) DEFAULT null,
  `nationality` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `season_team_coach` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `season_team_id` int(10),
  `coach_id` int(10),
  `name_cn` varchar(64) DEFAULT null,
  `name_en` varchar(64) DEFAULT null,
  `portrait` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_schedule` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `source_id` varchar(64) UNIQUE NOT NULL,
  `league_id` int(10),
  `match_info_id` int(10),
  `match_status_id` int(10),
  `play_by_play_status_id` int(10),
  `start_at` varchar(64) NOT NULL,
  `total_minutes` varchar(64) DEFAULT null,
  `arena_id` varchar(64) DEFAULT null,
  `neutral` tinyint(1) DEFAULT false,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_teams` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `match_id` int(10),
  `team_id` int(10),
  `team_type` ENUM ('home', 'away'),
  `formation` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_session_score` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `match_team_id` int(10),
  `session` varchar(64) NOT NULL,
  `score` int(4) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_team_stats` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `match_id` int(10),
  `match_team_id` int(10),
  `yellow_cards` varchar(64) DEFAULT "",
  `yellow_red_cards` varchar(64) DEFAULT "",
  `red_cards` varchar(64) DEFAULT "",
  `corner_kicks` varchar(64) DEFAULT "",
  `cards_given` varchar(64) DEFAULT "",
  `substitutions` varchar(64) DEFAULT "",
  `ball_possession` varchar(64) DEFAULT "",
  `free_kicks` varchar(64) DEFAULT "",
  `goal_kicks` varchar(64) DEFAULT "",
  `throw_ins` varchar(64) DEFAULT "",
  `offsides` varchar(64) DEFAULT "",
  `shots_total` varchar(64) DEFAULT "",
  `shots_on_target` varchar(64) DEFAULT "",
  `shots_off_target` varchar(64) DEFAULT "",
  `shots_saved` varchar(64) DEFAULT "",
  `fouls` varchar(64) DEFAULT "",
  `injuries` varchar(64) DEFAULT "",
  `shots_blocked` varchar(64) DEFAULT "",
  `out_of_bounds` varchar(64) DEFAULT "",
  `penalties` varchar(64) DEFAULT "",
  `penalties_missed` varchar(64) DEFAULT "",
  `own_goals` varchar(64) DEFAULT "",
  `attack` varchar(64) DEFAULT "",
  `dangerous_attack` varchar(64) DEFAULT "",
  `PK` varchar(64) DEFAULT "",
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_player_stats` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `match_id` int(10),
  `match_team_id` int(10),
  `player_id` int(10),
  `position_id` int(10),
  `starter` tinyint(1) DEFAULT false,
  `goals_scored` varchar(64) DEFAULT "",
  `goals_conceded` varchar(64) DEFAULT "",
  `goals_by_head` varchar(64) DEFAULT "",
  `goals_by_penalty` varchar(64) DEFAULT "",
  `own_goals` varchar(64) DEFAULT "",
  `red_cards` varchar(64) DEFAULT "",
  `yellow_cards` varchar(64) DEFAULT "",
  `yellow_red_cards` varchar(64) DEFAULT "",
  `assists` varchar(64) DEFAULT "",
  `substituted_in` varchar(64) DEFAULT "",
  `substituted_out` varchar(64) DEFAULT "",
  `clearances` varchar(64) DEFAULT "",
  `offsides` varchar(64) DEFAULT "",
  `interceptions` varchar(64) DEFAULT "",
  `chances_created` varchar(64) DEFAULT "",
  `crosses_successful` varchar(64) DEFAULT "",
  `crosses_total` varchar(64) DEFAULT "",
  `passes_total` varchar(64) DEFAULT "",
  `passes_successful` varchar(64) DEFAULT "",
  `passes_unsuccessful` varchar(64) DEFAULT "",
  `long_passes_total` varchar(64) DEFAULT "",
  `long_passes_successful` varchar(64) DEFAULT "",
  `long_passes_unsuccessful` varchar(64) DEFAULT "",
  `tackles_total` varchar(64) DEFAULT "",
  `tackles_successful` varchar(64) DEFAULT "",
  `shots_on_target` varchar(64) DEFAULT "",
  `shots_faced_saved` varchar(64) DEFAULT "",
  `shots_faced_total` varchar(64) DEFAULT "",
  `shots_blocked` varchar(64) DEFAULT "",
  `penalties_faced` varchar(64) DEFAULT "",
  `penalties_saved` varchar(64) DEFAULT "",
  `dribbles_completed` varchar(64) DEFAULT "",
  `loss_of_possession` varchar(64) DEFAULT "",
  `diving_saves` varchar(64) DEFAULT "",
  `fouls_committed` varchar(64) DEFAULT "",
  `was_fouled` varchar(64) DEFAULT "",
  `minutes_played` varchar(64) DEFAULT "",
  `defensive_blocks` varchar(64) DEFAULT "",
  `x_coordinates` varchar(64) DEFAULT "",
  `y_coordinates` varchar(64) DEFAULT "",
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_lineup` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `match_id` int(10) NOT NULL,
  `match_team_id` int(10) NOT NULL,
  `player_id` int(10) NOT NULL,
  `position_id` int(10) NOT NULL,
  `jersey_num` varchar(64) DEFAULT null,
  `starter` tinyint(1) DEFAULT false,
  `order` int(10) DEFAULT null,
  `played` tinyint(1) DEFAULT false,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_incident_events` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `match_id` int(10), 
  `source_id` varchar(64) DEFAULT null,
  `match_team_id` int(10),
  `team_type` ENUM ('home', 'away', 'neutral'),
  `incident_type_id` int(10),
  `incident_time` varchar(64),
  `player_id` int(10) DEFAULT null,
  `assist_player_id` int(10) DEFAULT null,
  `sub_in_player_id` int(10) DEFAULT null,
  `sub_out_player_id` int(10) DEFAULT null,
  `home_score` int(10) DEFAULT null,
  `away_score` int(10) DEFAULT null,
  `x_coordinate` varchar(64) DEFAULT null,
  `y_coordinate` varchar(64) DEFAULT null,
  `period` varchar(64) DEFAULT null,
  `period_type` varchar(64) DEFAULT null,
  `injury_time_announced` varchar(64) DEFAULT null,
  `description` varchar(64) DEFAULT null,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `match_status` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(64),
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `play_by_play_status` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `incident_type` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `api_records` (
  `id` int(10) PRIMARY KEY AUTO_INCREMENT,
  `collection_name` varchar(128) NOT NULL,
  `query_parameter` varchar(256) DEFAULT NULL,
  `language_code` ENUM ('en', 'zh'),
  `hash` varchar(128) NOT NULL,
  `object_id` varchar(64) NOT NULL,
  `doc_generated_at` int(10) DEFAULT "0",
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `league` ADD FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

ALTER TABLE `league_match_info` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `season_teams` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `season_teams` ADD FOREIGN KEY (`league_match_info_id`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `season_teams` ADD FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

ALTER TABLE `season_teams` ADD FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`);

ALTER TABLE `team_season_standing` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `team_season_standing` ADD FOREIGN KEY (`league_match_info_id`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `team_season_standing` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `team_season_stats` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `team_season_stats` ADD FOREIGN KEY (`league_match_info_id`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `team_season_stats` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `team_season_stats_top` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `team_season_stats_top` ADD FOREIGN KEY (`league_match_info_id`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `team_season_stats_top` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `team_season_stats_top` ADD FOREIGN KEY (`stat_type_id`) REFERENCES `team_stats_type` (`id`);

ALTER TABLE `team_stats_type` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `team_season_event` ADD FOREIGN KEY (`team_event_type_id`) REFERENCES `team_event_type` (`id`);

ALTER TABLE `team_season_event` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `player` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `season_team_roster` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `season_team_roster` ADD FOREIGN KEY (`player_id`) REFERENCES `player` (`id`);

ALTER TABLE `season_team_roster` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `season_team_roster` ADD FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`);

ALTER TABLE `player_season_stats` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `player_season_stats` ADD FOREIGN KEY (`league_match_info_id`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `player_season_stats` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `player_season_stats` ADD FOREIGN KEY (`season_player_id`) REFERENCES `season_team_roster` (`id`);

ALTER TABLE `player_season_stats` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `player_season_stats_top` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `player_season_stats_top` ADD FOREIGN KEY (`league_match_info_id`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `player_season_stats_top` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `player_season_stats_top` ADD FOREIGN KEY (`season_player_id`) REFERENCES `season_team_roster` (`id`);

ALTER TABLE `player_season_stats_top` ADD FOREIGN KEY (`stat_type_id`) REFERENCES `player_stats_type` (`id`);

ALTER TABLE `player_season_event` ADD FOREIGN KEY (`league_match_info`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `player_season_event` ADD FOREIGN KEY (`player_id`) REFERENCES `player` (`id`);

ALTER TABLE `player_season_event` ADD FOREIGN KEY (`event_type_id`) REFERENCES `player_event_type` (`id`);

ALTER TABLE `player_season_event` ADD FOREIGN KEY (`from_team`) REFERENCES `team` (`id`);

ALTER TABLE `player_season_event` ADD FOREIGN KEY (`to_team`) REFERENCES `team` (`id`);

ALTER TABLE `season_team_coach` ADD FOREIGN KEY (`season_team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `season_team_coach` ADD FOREIGN KEY (`coach_id`) REFERENCES `coach` (`id`);

ALTER TABLE `match_schedule` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `match_schedule` ADD FOREIGN KEY (`match_info_id`) REFERENCES `league_match_info` (`id`);

ALTER TABLE `match_schedule` ADD FOREIGN KEY (`match_status_id`) REFERENCES `match_status` (`id`);

ALTER TABLE `match_schedule` ADD FOREIGN KEY (`play_by_play_status_id`) REFERENCES `play_by_play_status` (`id`);

ALTER TABLE `match_teams` ADD FOREIGN KEY (`match_id`) REFERENCES `match_schedule` (`id`);

ALTER TABLE `match_teams` ADD FOREIGN KEY (`team_id`) REFERENCES `season_teams` (`id`);

ALTER TABLE `match_session_score` ADD FOREIGN KEY (`match_team_id`) REFERENCES `match_teams` (`id`);

ALTER TABLE `match_team_stats` ADD FOREIGN KEY (`match_id`) REFERENCES `match_schedule` (`id`);

ALTER TABLE `match_team_stats` ADD FOREIGN KEY (`match_team_id`) REFERENCES `match_teams` (`id`);

ALTER TABLE `match_player_stats` ADD FOREIGN KEY (`match_id`) REFERENCES `match_schedule` (`id`);

ALTER TABLE `match_player_stats` ADD FOREIGN KEY (`match_team_id`) REFERENCES `match_teams` (`id`);

ALTER TABLE `match_player_stats` ADD FOREIGN KEY (`player_id`) REFERENCES `season_team_roster` (`id`);

ALTER TABLE `match_player_stats` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `match_lineup` ADD FOREIGN KEY (`match_id`) REFERENCES `match_schedule` (`id`);

ALTER TABLE `match_lineup` ADD FOREIGN KEY (`match_team_id`) REFERENCES `match_teams` (`id`);

ALTER TABLE `match_lineup` ADD FOREIGN KEY (`player_id`) REFERENCES `season_team_roster` (`id`);

ALTER TABLE `match_lineup` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `match_incident_events` ADD FOREIGN KEY (`match_id`) REFERENCES `match_schedule` (`id`);

ALTER TABLE `match_incident_events` ADD FOREIGN KEY (`match_team_id`) REFERENCES `match_teams` (`id`);

ALTER TABLE `match_incident_events` ADD FOREIGN KEY (`incident_type_id`) REFERENCES `incident_type` (`id`);

CREATE INDEX `category_index_0` ON `category` (`source_id`);

CREATE INDEX `category_index_1` ON `category` (`country_id`);

CREATE INDEX `league_index_2` ON `league` (`source_id`);

CREATE INDEX `league_match_info_index_3` ON `league_match_info` (`league_id`);

CREATE INDEX `league_match_info_index_4` ON `league_match_info` (`source_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `league_match_info` (`league_id`, `source_id`);

CREATE INDEX `arena_index_6` ON `arena` (`source_id`);

CREATE INDEX `arena_index_7` ON `arena` (`country_id`);

CREATE INDEX `country_index_8` ON `country` (`source_id`);

CREATE INDEX `team_index_9` ON `team` (`source_id`);

CREATE INDEX `season_teams_index_10` ON `season_teams` (`league_id`);

CREATE INDEX `season_teams_index_11` ON `season_teams` (`league_match_info_id`);

CREATE INDEX `season_teams_index_12` ON `season_teams` (`team_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `season_teams` (`league_id`, `league_match_info_id`, `team_id`);

CREATE INDEX `team_season_standing_index_14` ON `team_season_standing` (`league_id`);

CREATE INDEX `team_season_standing_index_15` ON `team_season_standing` (`league_match_info_id`);

CREATE INDEX `team_season_standing_index_16` ON `team_season_standing` (`season_team_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `team_season_standing` (`league_id`, `league_match_info_id`, `season_team_id`);

CREATE INDEX `team_season_stats_index_18` ON `team_season_stats` (`league_id`);

CREATE INDEX `team_season_stats_index_19` ON `team_season_stats` (`league_match_info_id`);

CREATE INDEX `team_season_stats_index_20` ON `team_season_stats` (`season_team_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `team_season_stats` (`league_id`, `league_match_info_id`, `season_team_id`);

CREATE INDEX `team_season_stats_top_index_22` ON `team_season_stats_top` (`league_id`);

CREATE INDEX `team_season_stats_top_index_23` ON `team_season_stats_top` (`league_match_info_id`);

CREATE INDEX `team_season_stats_top_index_24` ON `team_season_stats_top` (`season_team_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `team_season_stats_top` (`league_id`, `league_match_info_id`, `season_team_id`, `stat_type_id`);

CREATE INDEX `player_index_26` ON `player` (`source_id`);

CREATE INDEX `season_team_roster_index_27` ON `season_team_roster` (`season_team_id`);

CREATE INDEX `season_team_roster_index_28` ON `season_team_roster` (`player_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `season_team_roster` (`season_team_id`, `player_id`);

CREATE INDEX `player_season_stats_index_30` ON `player_season_stats` (`league_id`);

CREATE INDEX `player_season_stats_index_31` ON `player_season_stats` (`league_match_info_id`);

CREATE INDEX `player_season_stats_index_32` ON `player_season_stats` (`season_team_id`);

CREATE INDEX `player_season_stats_index_33` ON `player_season_stats` (`season_player_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `player_season_stats` (`league_id`, `league_match_info_id`, `season_team_id`, `season_player_id`);

CREATE INDEX `player_season_stats_top_index_35` ON `player_season_stats_top` (`league_id`);

CREATE INDEX `player_season_stats_top_index_36` ON `player_season_stats_top` (`league_match_info_id`);

CREATE INDEX `player_season_stats_top_index_37` ON `player_season_stats_top` (`season_team_id`);

CREATE INDEX `player_season_stats_top_index_38` ON `player_season_stats_top` (`season_player_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `player_season_stats_top` (`league_id`, `league_match_info_id`, `season_team_id`, `season_player_id`, `stat_type_id`);

CREATE INDEX `coach_index_40` ON `coach` (`source_id`);

CREATE INDEX `season_team_coach_index_41` ON `season_team_coach` (`season_team_id`);

CREATE INDEX `season_team_coach_index_42` ON `season_team_coach` (`coach_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `season_team_coach` (`season_team_id`, `coach_id`);

CREATE INDEX `match_schedule_index_44` ON `match_schedule` (`source_id`);

CREATE INDEX `match_schedule_index_45` ON `match_schedule` (`league_id`);

CREATE INDEX `match_schedule_index_46` ON `match_schedule` (`match_info_id`);

CREATE INDEX `match_schedule_index_47` ON `match_schedule` (`start_at`);

CREATE INDEX `match_teams_index_48` ON `match_teams` (`match_id`);

CREATE INDEX `match_teams_index_49` ON `match_teams` (`team_id`);

CREATE INDEX `match_teams_index_50` ON `match_teams` (`team_type`);

CREATE UNIQUE INDEX `composite_unique_index` ON `match_teams` (`match_id`, `team_id`);

CREATE INDEX `match_session_score_index_52` ON `match_session_score` (`match_team_id`);

CREATE INDEX `match_session_score_index_53` ON `match_session_score` (`session`);

CREATE UNIQUE INDEX `composite_unique_index` ON `match_session_score` (`match_team_id`, `session`);

CREATE UNIQUE INDEX `composite_unique_index` ON `match_lineup` (`match_id`, `match_team_id`, `player_id`);

CREATE INDEX `match_team_stats_index_55` ON `match_team_stats` (`match_id`);

CREATE INDEX `match_team_stats_index_56` ON `match_team_stats` (`match_team_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `match_team_stats` (`match_id`, `match_team_id`);

CREATE INDEX `match_player_stats_index_58` ON `match_player_stats` (`match_id`);

CREATE INDEX `match_player_stats_index_59` ON `match_player_stats` (`match_team_id`);

CREATE INDEX `match_player_stats_index_60` ON `match_player_stats` (`player_id`);

CREATE INDEX `api_records_index1` ON `api_records` (`collection_name`, `query_parameter`, `language_code`);

CREATE UNIQUE INDEX `composite_unique_index` ON `api_records` (`collection_name`, `query_parameter`, `language_code`);

CREATE UNIQUE INDEX `composite_unique_index` ON `match_player_stats` (`match_id`, `match_team_id`, `player_id`);

CREATE UNIQUE INDEX `composite_unique_index` ON `match_incident_events` (`match_id`, `incident_type_id`, `incident_time`);

-- ALTER TABLE `league` COMMENT = "    - The API doesn't offer the enough information, so it must be add information by self.

-- ## API_Source:
--     - Sportsradar: competition_info";

-- ALTER TABLE `league_match_info` COMMENT = "    - This is parent-child table.

-- ### API_Source:
--     - Sportsradar: competition_season, seasons(all)";

-- ALTER TABLE `arena` COMMENT = "    - There isn't an API whcih can offer the arena information.

-- ### API_Source:
--     - Sportsradar: competitor_profile";

-- ALTER TABLE `country` COMMENT = "    - There isn't an API whcih can offer the country information.

-- ### API_Source:
--     - Sportsradar: competitor_profile [from venue attribute]";

-- ALTER TABLE `currency` COMMENT = "    - Define currency type, it uses for the market value of team and player.

-- ### API_Source:
--     - ";

-- ALTER TABLE `team` COMMENT = "    - It records all teams.

-- ### API_Source:
--     - Sportsradar: competitor_profile";

-- ALTER TABLE `season_teams` COMMENT = "    - many-to-many table, It refers from team and league_match_info table.

-- ### API_Source:
--     - Sportsradar: season_summaries";

-- ALTER TABLE `team_season_standing` COMMENT = "### API_Source:
--     - Sportsradar: season_standing";

-- ALTER TABLE `team_season_stats` COMMENT = "### API_Source:
--     - Sportsradar: season_competitor_statistics";

-- ALTER TABLE `team_season_stats_top` COMMENT = "- Need to calculate by ourself, and it calculates from team_sesaon_stats.";

-- ALTER TABLE `team_season_event` COMMENT = "- It define by ourself.";

-- ALTER TABLE `team_event_type` COMMENT = "These static values are for describing the event in which the team occurs.";

-- ALTER TABLE `position` COMMENT = "- Define by ourself, we need to map the position that API offer.";

-- ALTER TABLE `player` COMMENT = "### API Source:
--     - Sportsradar: player_profile";

-- ALTER TABLE `season_team_roster` COMMENT = "    - The table consists of season_teams and player.

-- ### API Source:
--     - Sportsradar: player_profile, historical -> according season_summaries to get team roster";

-- ALTER TABLE `player_season_stats` COMMENT = "### API Source:
--     - Sportsradar: season_competitor_statistics";

-- ALTER TABLE `player_season_stats_top` COMMENT = "## API:
--     - Sportsradar: season_leaders";

-- ALTER TABLE `coach` COMMENT = "### API_Source:
--     - Sportsradar: competitor_profile";

-- ALTER TABLE `season_team_coach` COMMENT = "### API_Source:
--     - Sportsradar: competitor_profile";

-- ALTER TABLE `match_schedule` COMMENT = "    - many-to-many table, It refers from league and league_match_info table.

-- ## API Source:
--     - Sportsradar: seaon_schedule";

-- ALTER TABLE `match_teams` COMMENT = "## API_Source:
--     - Sportsradar: season_schedule";

-- ALTER TABLE `match_session_score` COMMENT = "## API_Source:
--     - Sportsradar: current -> sports_event_lineups, historocal -> seaon_summaries (period_scores)";

-- ALTER TABLE `match_team_stats` COMMENT = "## API_Source:
--     - Sportsradar: current -> sports_event_summary or live_summaries(but need to filter), 
--                 historocal -> seaon_summaries";

-- ALTER TABLE `match_player_stats` COMMENT = "## API_Source:
--     - Sportsradar: Current -> sports_event_lineups, sports_event_summary or live_summaries(but need to filter),
--                  historical -> season_linups, seaon_summaries";

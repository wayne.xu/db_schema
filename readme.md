# Database Schema

## Use DBML(database markup language) to define database structures. 

 - Here is dbml full syntax [docs](https://www.dbml.org/docs/#project-definition).


## Why do we use dbml to define the database structures?

 - It can use [dbml-cli](https://www.dbml.org/cli/) to decode dbml file to another SQL language, like MySQL, PostgreSQL and SQL Server.
 
 - Can use [dbdiagram.io](https://dbdiagram.io/d) to visualize the realationship between each table.
 
- Can easily use version control tool to manage db schema.


## dbml-cli example

 - Decode to MySQL script as an example:
```
dbml2sql file_you_want_to_decode --mysql -o output_file
```

## Bug

When I use [dbml-cli](https://www.dbml.org/cli/) to generate the SQL script, there will be a wrong value which I set the attribute of the field is datetime type and default value is ***"CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"***.

## Bug Fix

Use Python script to fix the problem and it will generate a new file which file name will be add the prefix ***fixed_***. 

```console
python fix_dbml2sql.py -f sql_file_you_want_to_be_fixed
```

## Log

  - 2020-10-21:
    - Schema version 2
    - Add new table category, and modify league.country_id to category_id which refer from category.id
  
  - 2020-10-22
    - Add new filed parent_id to league table.

  - 2020-10-30
    - Add unique key to Arena, Country, and MatchSchedule tables.
    - Modify session field in MatchSessionScore table.

  - 2020-11-06
    - Add new columns to team_season_standing, and modify its column name.

  - 2020-11-10
    - Modify player stats type table.
    - Modify player player season stats top composite unique index.

  - 2020-11-10
    - Fix error colname of match schedule. 
  
  - 2020-11-17
    - Add new fields to match_incident_events.

  - 2020-11-18
    - Add composite unique index to match_incident_events.

  - 2020-11-23
    - Add two new tables match_lineup and match_coach.

  - 2020-12-16
    - Modify the length of name_cn, name_en, logo and confederation in the league table.

  - 2020-12-17
    - Modify the length of name in the league_match_info table.

  - 2020-12-24
    - Add new table api_records to record api raw data version.

  - 2020-12-28
    - Add new column to api_records.

  - 2020-12-29
    - Modify api_records field name from api_name to colleciton_name.

  - 2021-01-04
    - Modify league_match_info name to name_en and name_cn.

  - 2021-01-29
    - Modify the fields name in the match_incident_event table.

  - 2021-02-01
    - Modify the data type of APIRecord table.
